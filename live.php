<?php
include 'fb.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
 <!-- CSS -->
 <link rel="stylesheet" href="style.css">
 <!-- <link rel ="title icon" type="img/png" href="img/commandements.png"> -->
 <!-- Bootstrap CDN  -->
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
     crossorigin="anonymous">
     <!--jq-->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <!-- <script src="jquery-3.3.1.min"></script>  -->
<!--icons-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
 <!-- JQ -->
 <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
 <!-- Bootstrap CDN END -->
 <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
 <title><?php echo $lang["NAWAL EDDIBOUCH"];?></title>
</head>
<body class="body">
  <!--start header-->
   <header id="home">
 <!--start nav-->
 <!-- <nav class="navbar navbar-expand-lg navbar-transparent bg-transparent mar">
    <a class="navbar-brand ml-5 text-uppercase span" href="#"><?php echo $lang["portfolio"];?></a>
    <button class="navbar-toggler bg-secondary" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <center>
    <div class="collapse navbar-collapse" id="navbarNav">      
    <ul class="nav float-center">
        <li class="nav-item active">
          <a class="nav-link ml-2 mt-2 text-uppercase" href="#propos"><?php echo $lang["a propos de moi"];?> <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link ml-2 mt-2 text-uppercase" href="#expérience"><?php echo $lang["expérience"];?></a>
        </li> -->
        <!-- <li class="nav-item">
          <a class="nav-link ml-2 mt-2 text-uppercase" href="#education"><?php echo $lang["education"];?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link ml-2 mt-2 text-uppercase" href="#portfolio"><?php echo $lang["portfolio"];?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link ml-2 mt-2 text-uppercase" href="#contact"><?php echo $lang["contact"];?></a>
          </li>
      </ul>
    </div>  
  </nav> -->
  <!-- </center>  --> -->
 <!--end nav-->


 <nav class="navbar navbar-expand-lg bg-light fixed-top" id="mainNav">
        <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#">
      <img src="img/E_1.png" width="60" height="60" alt="">
    </a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link active js-scroll-trigger pl-3 text-uppercase" href="#home"><?php echo $lang["home"];?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link active js-scroll-trigger pl-3 text-uppercase" href="#propos"><?php echo $lang["a propos de moi"];?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger pl-3 text-uppercase" href="#expérience"><?php echo $lang["expérience"];?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger pl-3 text-uppercase" href="#education"><?php echo $lang["education"];?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger pl-3 text-uppercase" href="#portfolio"><?php echo $lang["portfolio"];?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger pl-3 text-uppercase" href="#contact"><?php echo $lang["contact"];?></a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div data-aos="zoom-in"><h1 class="text-center text-light text-justify px-2 pt-3"><?php echo $lang["Salut, Je m'appelle"];?> <span><?php echo $lang["Nawal"];?> <br><?php echo $lang["Eddibouch"];?></span></h1></div>
 <div data-aos="zoom-in"><p class="text-center text-light mt-4"><?php echo $lang["Je Suis Développeuse Web"];?></p></div>
 <center><div data-aos="zoom-in-up"><a href="doc/NAWAL EDDIBOUCHCV.docx" class="btn px-4 py-2 my-4 btn-sm butd justify-content-center text-light"><?php echo $lang["Download CV"];?> <i class="fas fa-download text-light p-1"></i></a></div></center>
 <!-- start language -->
<div>
 <a href="live.php?lang=fr"><img src="img/téléchargement.png" width="30rem" height="30rem" class="fixe" style="position:fixed; z-index:5"></a><br>
<a href="live.php?lang=en"><img src="img/fr (2).jpg" width="30rem" height="30rem" class="fixe" style="position:fixed; z-index:5"></a>
</div>
<!-- end language -->
</header>
<!--end header-->
<!-- start section -->
<section id="propos" class="my-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-10">
            <div data-aos="zoom-in-down"> <img src="img/PH (2).png" alt="photo" class="mt-2 mb-2"></div>
            </div>           
            <div class="col-lg-8 col-md-8 col-sm-12">
            <div data-aos="zoom-in"><h2><?php echo $lang["A propos de Moi"];?></h2></div> <hr>
<p class="text-justify text-muted text-capitalize px-2"><?php echo $lang["para1"];?><span class="one"><?php echo $lang["SOL"];?></span><span class="two"><?php echo $lang["O"];?> </span><span class="three"> <?php echo $lang["LEARN"];?></span> <?php echo $lang["language"];?>
 <?php echo $lang["para2"];?> <span class="four"> <?php echo $lang["Udacity"];?> </span> <?php echo $lang["para3"];?> 
 </p>
 <div data-aos="zoom-in"><h2><?php echo $lang["Information Personnel"];?></h2></div><hr>
<ul>
    <li class="text-muted"><?php echo $lang["Nom et prénom : EDDIBOUCH NAWAL"];?></li>
    <li class="text-muted"><?php echo $lang["Age : 34 ans"];?></li>
    <li class="text-muted"><?php echo $lang["Tél : 06 26 71 39 93"];?></li>
    <li class="text-muted"><?php echo $lang["email"];?></li>
    <li class="text-muted"> <?php echo $lang["addre"];?></li>
</ul>
<div data-aos="zoom-in-up"><a href="https://gitlab.com/youcode.ma" class="btn btn-dark px-4 py-2 btn-sm butd"> <?php echo $lang["Git Hub"];?> <i class="fab fa-gitlab text-light"></i></a></div>
            </div>
        </div>
    </div>
</section>
<!-- end section1 -->
<!-- start section2 -->
        <section id="section2" class="my-5">
                    <div class="container">
                    <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12">
                    <div data-aos="zoom-in"><h2 class="text-uppercase"><?php echo $lang["skills"];?></h2></div>
                    </div> 
                    <div class="col-lg-8 col-md-8 col-sm-12">                              
                    <p class="py-2">HTML5</p>    
                    <div class="progress" style="height: 0.75rem;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-dark" role="progressbar" style="width: 98%;" aria-valuenow="98" aria-valuemin="0" aria-valuemax="10">98%</div>
                    </div>
                    <p class="py-2">CSS3</p>
                    <div class="progress" style="height: 0.75rem;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 95%;" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100">90%</div>
                    </div>
                    <p class="py-2">BOOTSTRAP</p>
                    <div class="progress" style="height: 0.75rem;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-dark" role="progressbar" style="width: 88%;" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100">88%</div>
                    </div>
                    <p class="py-2">JAVA SCRIPT</p>
                    <div class="progress" style="height: 0.75rem;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 92%;" aria-valuenow="92" aria-valuemin="0" aria-valuemax="100">92%</div>
                    </div>
                    <p class="py-2">JEQUERY</p>
                    <div class="progress" style="height: 0.75rem;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-dark" role="progressbar" style="width: 60%;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%</div>
                    </div>
                    <p class="py-2">Sql</p>
                    <div class="progress" style="height: 0.75rem;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">70%</div>
                    </div>
                    <p class="py-2">PHP</p>
                    <div class="progress" style="height: 0.75rem;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-dark" role="progressbar" style="width: 60%; height: 10px" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%</div>
                    </div>    
                    <p class="py-1">ADOBE XD</p>
                    <div class="progress" style="height: 0.75rem;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 60%;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%</div>
                    </div>  
                                </div>
                            </div>
                        </div>   
</section>
<!-- end section2 -->
<!-- start section3 -->
              <section id="expérience" class="pb-5">
              <div data-aos="zoom-in"><h2 class="text-center my-5 pt-5 px-2"><?php echo $lang["Expérience"];?> <span class="five"> <?php echo $lang["Professionnelle"];?></span></h2></div>
              <div class="container">
              <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 parag">
              <p class="text-justify text-capitalize text-muted bg-light px-5 py-5" data-aos="zoom-in-up"> <?php echo $lang["para4"];?> <span class="one"> <?php echo $lang["SOL"];?></span><span class="two"><?php echo $lang["O"];?></span><span class="three"><?php echo $lang["LEARN"];?></span> .
              <?php echo $lang["para5"];?></p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 parag">
              <p class="text-justify text-capitalize text-muted bg-light px-5 py-5" data-aos="zoom-in-up"><?php echo $lang["para6"];?></p>
              </div>
              </div>
              </div>
              <div class="container">
              <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 parag">
              <p class="text-justify text-muted text-capitalize bg-light px-5 py-5 par" data-aos="zoom-in-up"><?php echo $lang["para7"];?></p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 parag">
              <p class="text-justify text-muted text-capitalize bg-light px-5 py-5" data-aos="zoom-in-up"><?php echo $lang["para8"];?></p>
              </div>
              </div>
              </div> 
              <div class="container">
              <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 parag">
              <p class="text-justify text-muted text-capitalize bg-light px-5 py-5" data-aos="zoom-in-up"><?php echo $lang["para9"];?>
               <span class="four"> <?php echo $lang["Udacity"];?></span><?php echo $lang["para10"];?></p>
              </div> 
              <div class="col-lg-6 col-md-6 col-sm-12 parag">
              <p class="text-justify text-muted text-capitalize bg-light px-5 py-5" data-aos="zoom-in-up"><?php echo $lang["para11"];?></p>
              </div>
              </div>             
              </div>  
            
                </div> 
</section>
<!-- end section3 -->
           <!-- start sectionportfolio -->
<section id="portfolio" class="pb-5">
          <div data-aos="zoom-in"><h2 class="text-center my-4 pt-3"><?php echo $lang["Portfolio"];?></span></h2></div>  
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                <a href="https://gitlab.com/nwalalfadly8/portfolio">
                <div data-aos="zoom-in"><img src="img/37.png" alt="photo" class="img"> </div>
              </a>
                <p class="text-capitalize text-center pt-4 pb-3 text-muted">Atelier<br>
                    <small class="text-muted text-center">HTML CSS3</small></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                <a href="https://gitlab.com/nwalalfadly8/html-css-challenge">
                <div data-aos="zoom-in"> <img src="img/41.png" alt="photo" class="img"> </div>
                </a>
                        <p class="text-capitalize text-center pt-4 pb-3 text-muted">Atelier Slack<br>
                            <small class="text-muted text-center">HTML CSS3 </small></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                <a href="https://gitlab.com/nwalalfadly8/js">
                <div data-aos="zoom-in"> <img src="img/15.png" alt="photo" class="img"> </div>
                  </a>
                  <p class="text-capitalize text-center pt-4 pb-3 text-muted">Atelier<br>
                    <small class="text-muted text-center">HTML CSS3</small></p>
                </div>
                </div>
            <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12">
                    <a href="https://gitlab.com/nwalalfadly8/trainning">
                    <div data-aos="zoom-in"><img src="img/5.png" alt="photo" class="img"> </div>
                  </a>
                    <p class="text-capitalize text-center pt-4 pb-3 text-muted">Site Youcode<br>
                        <small class="text-muted text-center">HTML, CSS3 Et BOOTSTRAP</small></p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                    <a href="https://gitlab.com/nwalalfadly8/nawalsiteweb">
                    <div data-aos="zoom-in"> <img src="img/123.png" alt="photo" class="img"> </div>
                    </a>
                            <p class="text-capitalize text-center pt-4 pb-3 text-muted">Atelier<br>
                                <small class="text-muted text-center">HTML Et CSS3</small></p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                    <a href="https://gitlab.com/nwalalfadly8/html-css-challenge">
                    <div data-aos="zoom-in"> <img src="img/20.png" alt="photo" class="img"> </div>
                  </a>
                            <p class="text-capitalize text-center pt-4 pb-3 text-muted">Site Le Sucre Quotidienne<br>
                                <small class="text-muted text-center">HTML, CSS3, BOOTSTRAP Et JQ</small></p>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12">
                        <a href="https://gitlab.com/nwalalfadly8/nawalsiteweb">
                        <div data-aos="zoom-in"><img src="img/17.png" alt="photo" class="img"> </div>
                      </a>
                        <p class="text-capitalize text-center pt-4 pb-3 text-muted">Site Le Patisserie Quotidienne<br>
                            <small class="text-muted text-center">HTML, CSS3, BOOTSTRAP Et JQ</small></p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                          <a href="">
                          <div data-aos="zoom-in"><img src="img/25.png" alt="photo" class="img"> </div>
                          
                          </a>
                                <p class="text-capitalize text-center pt-4 pb-3 text-muted">Désign<br>
                                <small class="text-muted text-center">ADOBE XD</small></p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                        <a href="https://gitlab.com/nwalalfadly8/cv_xd">
                        <div data-aos="zoom-in"><img src="img/12.png" alt="photo" class="img"> </div>
                      </a>
                                <p class="text-capitalize text-center pt-4 pb-3 text-muted">CV<br>
                                    <small class="text-muted text-center">ADOBE XD</small></p>
                        </div>
                        </div>
                  </div>   
                                          
</section>
<!-- end sectionportfolio -->
<!-- start sectioneducation -->
<section id="education" class="mt-4 pt-3 pb-5">
               <div data-aos="zoom-in"><h2 class="text-center py-5"><?php echo $lang["Education"];?></h2></div>
                <div class="container">
                <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 parag">
                <p class="text-justify text-capitalize text-muted bg-light px-5 py-5" data-aos="zoom-in-up"> <?php echo $lang["DECEMBRE 2018 - PRESENT."];?><br>
                <span class="text-dark"> <?php echo $lang["para12"];?> <span class="one"><?php echo $lang["SOL"];?></span><span class="two"><?php echo $lang["O"];?></span><span class="three"><?php echo $lang["LEARN"];?></span> .</span><br>
                <?php echo $lang["para13"];?><br>                       
                <small class="text-muted text-center"><?php echo $lang["SOLOLEARN"];?></small></p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 parag">
                <p class="text-justify text-capitalize text-muted bg-light px-5 py-5" data-aos="zoom-in-up"> <?php echo $lang["OCTOBRE"];?><br><br>
                <span class="text-dark"><?php echo $lang["Semmlalia"];?></span><br>                       
                <?php echo $lang["SVT"];?><br><br>                      
                <small class="text-muted text-center"><?php echo $lang["Faculté"];?></small></p>
                </div>
                </div>
                <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 parag">
                <p class="text-justify text-capitalize text-muted bg-light px-5 py-5" data-aos="zoom-in-up"> <?php echo $lang["28 - 29 AVRIL 2018"];?><br>
                <span class="text-dark"> <?php echo $lang["Calcul"];?></span><br>                      
                <?php echo $lang["para13"];?><br>                       
                <small class="text-muted text-center"><?php echo $lang["Ebel"];?></small> </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 parag">
                <p class="text-justify text-capitalize text-muted bg-light px-5 py-5" data-aos="zoom-in-up"><?php echo $lang["JUILLET"];?><br>
                <span class="text-dark"><?php echo $lang["Baccalauréat en SVT"];?></span><br>                        
                <?php echo $lang["para14"];?><br><br>                       
                <small class="text-muted text-center"><?php echo $lang["Bac Libre Youssoufia"];?></small>  </p>
                </div>
                </div>
                <div class="row pb-4">
                <div class="col-lg-6 col-md-6 col-sm-12 parag">
                <p class="text-justify text-capitalize text-muted bg-light px-5 py-5" data-aos="zoom-in-up"><?php echo $lang["JUILLET"];?><br>
                <span class="text-dark"><?php echo $lang["para15"];?></span><br>                    
                <?php echo $lang["para16"];?><br>                       
                <small class="text-muted text-center"><?php echo $lang["para17"];?></small> </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 parag">
                <p class="text-justify text-capitalize text-muted bg-light px-5 py-5" data-aos="zoom-in-up"><?php echo $lang["JUIN"];?><br>
                <span class="text-dark"><?php echo $lang["para18"];?></span><br><br>        
                <?php echo $lang["para19"];?><br><br>                       
                <small class="text-muted text-center my-4"><?php echo $lang["Lycée"];?></small>  </p>
                </div>
                </div>
                </div>
                </section>


                <div data-aos="zoom-in-up"><h2 class="py-5 text-center carouse">Certifecats</h2></div>
                <div id="carouselExampleInterval" class="carousel slide container" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active" data-interval="100">
              <img src="img/img/img (7).png" class="d-block w-100 mb-5" alt="html">
            </div>
            <div class="carousel-item" data-interval="200">
            <img src="img/img/img (5).png" class="d-block w-100 mb-5" alt="css">
            </div>
            <div class="carousel-item">
            <img src="img/img/img (6).png" class="d-block w-100 mb-5" alt="js">
            </div>
            <div class="carousel-item" data-interval="100">
              <img src="img/img/img (1).png" class="d-block w-100 mb-5" alt="jq">
            </div>
            <div class="carousel-item" data-interval="200">
            <img src="img/img/img (4).png" class="d-block w-100 mb-5" alt="php">
            </div>
            <div class="carousel-item">
            <img src="img/img/img (2).png" class="d-block w-100 mb-5" alt="udacity">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
<!-- end section5 -->   
 <?php

    //Create variables

    $host="localhost";
    $user="root";
    $pw="";
    $ndb="one";
    $con=mysqli_connect($host,$user,$pw,$ndb);

    if ($_SERVER['REQUEST_METHOD'] == 'POST'){

    //Create variables And Function

    $nom = strtolower(filter_var($_POST['nom'], FILTER_SANITIZE_STRING));
    $email = strtolower(filter_var($_POST['email'], FILTER_SANITIZE_EMAIL));
    $message = strtolower(filter_var($_POST['message'], FILTER_SANITIZE_STRING));
    $query="INSERT INTO two (nom,email,message)value('$nom','$email','$message')";
     mysqli_query($con,$query);

 //Email Variables

      $toEmail = 'nwalalfadly8@gmail.com';
      $subject = 'Contact request From '.$nom;
      $body = '<h2>Contact Request</h2>
              <h4>Name</h4><p>'.$nom.'</p>
              <h4>Email</h4><p>'.$email.'</p>
              <h4>Subject</h4><p>'.$subject.'</p>
              <h4>Message</h4><p>'.$message.'</p>
              ';

     //Email Headers

          $headers = "MIME-Version: 1.0" ."\r\n";
          $headers .="Content-Type:text/html;charset=UTF-8" . " \r\n";

     //additional header

          $headers .= "from: " .$nom. "<".$email.">". "\r\n";
          if(mail($toEmail, $subject, $body, $headers)){

     //email send

              $msg = 'email send';
              $msgClass = 'alert-success';
          } else {
              $msg = 'not send';
              $msgClass = 'alert-danger';
          }

    //  create array

    $formErrors = array();
   if(strlen($nom)<3) {
    $formErrors[] = "<div class='alert alert-danger custom-alert container mb-3 text-center div'><h6> Ecrire Ton Nom S'il vous Plait</h6> </div>";
   }
   if(empty($email)){
    $formErrors[] = "<div class='alert alert-danger custom-alert container mb-3 text-center div'> <h6>Ecrire Ton E-mail S'il Vous Plait </h6></div>";
  }
  if(strlen($message)<= 9) {
    $formErrors[] = "<div class='alert alert-danger custom-alert container mb-3 text-center div'><h6> Il Faut Ecrire Plus 9 Caractérs S'il vous Plait</h6> </div>";
   }
   if(strlen($message)>= 250) {
    $formErrors[] = "<div class='alert alert-danger custom-alert container mb-3 text-center div'><h6> Il Faut Ecrire Moins 250 Caractérs S'il vous Plait</h6> </div>";
   }

}
?>
<!-- start footer -->
<footer id="contact">
<div data-aos="zoom-in-down"> <h2 class="text-center py-5"><?php echo $lang["CONTACT"];?></h2></div>
    <div class="container">
    <div class="row">  
    <div class="col-lg-6 col-md-6 col-sm-12">  

 <!-- CREATE FORM -->

    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
   
   <!-- CREATE ERROR BOOCLE-->

          <div class="error">
          <?php 
          if(isset($formErrors)){
          foreach($formErrors as $error){
              echo $error . '<br>';
          }
        }
          ?>
        </div>

    <!-- END ERROR -->

        <div class="input-group mb-3">
        <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="far fa-user"></i></span>
        </div>
        <input type="text" class="form-control" placeholder="Nom" aria-label="Nom" aria-describedby="basic-addon1" name="nom">
        </div>
        <div class="input-group mb-3">
        <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="far fa-envelope"></i></span>
        </div>
        <input type="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="basic-addon1" name="email">
        </div>
        <div class="input-group">
        <textarea class="form-control" cols="30" rows="10" aria-label="Message" name="message" placeholder="Message"></textarea>
        </div>     
        <br>
        <a href="#"><button class="btn btn-dark px-4 py-2 mb-5 btn-sm" name="click"> <i class="far fa-paper-plane mssg"></i> <?php echo $lang["Click"];?></button></a>                                                              
        </div>
      </form>
      <div class="col-lg-6 col-md-6 col-sm-12 text-light">  
      <div data-aos="zoom-in-down"><h3 class="py-3"><?php echo $lang["Details"];?></h3> </div>
        <span class="py-1 text-light"><?php echo $lang["EMAIL"];?></span>
        <p> <a href="nwalalfadly8@gmail.com"><?php echo $lang["nwalalfadly8@gmail.com"];?></a></p>
        <span class="py-1 text-light"><?php echo $lang["TÉL"];?></span>
        <p classs="text-muted">06 26 71 39 93 .</p> 
        <span class="my-3 text-light"><?php echo $lang["ADDRESS"];?></span>
        <p classs="text-muted"><?php echo $lang["Quartier"];?><br>
         <?php echo $lang["Rue"];?><br>
        <?php echo $lang["ville"];?></p><br>
        </div>
        </div>
        </div>
        <p class="text-center foot py-5">            
<a href="https://www.facebook.com/nawal.eddibouch" class="social-item my-5 px-3"><i class="fab fa-facebook-f px-4 py-3 icon i1" data-aos="flip-left"></i></a>
<a href="https://www.linkedin.com/in/nawal-eddibouch-995893173/" class="social-item my-5 px-3"><i class="fab fa-linkedin p-3 icon i2" data-aos="flip-left"></i></a>  
<a href="https://gitlab.com/youcode.ma" class="social-item my-5 px-3"><i class="fab fa-gitlab p-3 icon i3" data-aos="flip-left"></i></a>
<a href="https://outlook.live.com/mail/inbox" class="social-item my-5 px-3"><i class="far fa-envelope p-3 icon i3" data-aos="flip-left"></i></a>   <br>
<small class="text-center text-muted">Copy right &copy; <?php echo date("Y")?></small>  
<a href="mailto:tahre1993@gmail.com?Subject=Hello%20again" target="_top"></a>       
                  </p>
        </footer>
<!-- end footer -->
<script src="jq.js"></script>
   <!-- Bootstrap JS -->
<!-- <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
crossorigin="anonymous"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
</body>
</html>